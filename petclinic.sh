#!/bin/bash
aws s3 --region eu-west-1 cp s3://liban/spring-petclinic-2.0.0.jar spring-petclinic-2.0.0.jar

echo "DBSERVERNAME=$DBSERVERNAME"
echo "DBUSERNAME=$DBUSERNAME"
echo "DBPASSWORD=$DBPASSWORD"

cd /app
mkdir config

sed -e "s/DBSERVERNAME/$DBSERVERNAME/" \
    -e "s/DBUSERNAME/$DBUSERNAME/" \
    -e "s/DBPASSWORD/$DBPASSWORD/" application.properties.tmplt >config/application.properties

cat config/application.properties
counter=0
while ! mysql -h $DBSERVERNAME -u $DBUSERNAME -p$DBPASSWORD -e "show databases;"
do
  if ((counter==6))
  then
    exit 1
  fi

  sleep 10
  ((counter=counter+1))

done

java -jar ./spring-petclinic-2.0.0.jar
